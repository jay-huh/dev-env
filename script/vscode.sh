#!/bin/bash

APT="apt-get"

if [[ -z "${CI}" ]] ; then
	APT="apt"
fi

echo "Installing VS Code..."

#curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
#sudo apt-add-repository \
#  https://packages.microsoft.com/ubuntu/$(lsb_release -rs)/prod

wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

sudo ${APT} update -qq

sudo ${APT} ${APT_CACHE_OPTION} install -y -qq code
