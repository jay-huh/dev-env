#!/bin/bash

APT="apt-get"

if [[ -z "${CI}" ]] ; then
	APT="apt"
fi

echo "Installing Python 3.9..."

sudo ${APT} ${APT_CACHE_OPTION} install -y -qq python3.9-dev python-is-python3

#sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1
#sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 2

#sudo update-alternatives --config python3
